__OWNER__ = 'Chevron Corporation'
__AUTHOR__ = 'Ronaldo Mata'

import os
import pandas as pd

class JoinCsv():
	dfs = []
	data = None
	COLS = {
		'PID': 'Pozo', 
		'Date': 'Fecha', 
		'PIP': 'Pip', 
		'RPM': 'Rpm'
	}

	def __init__(self, dir_input, dir_output, file_name='fulldata.csv'):
		self.dir_input = dir_input
		self.dir_output = dir_output
		self.file_name = file_name

		self._get_data()

	def _message_info(self, filename):
		print("Processing {}...".format(filename))

	def _get_data(self):
		files = os.listdir(self.dir_input)
		for filename in files:

			self._message_info(filename)

			if not 'PUMP_PRESSURE' in filename:
				continue
			if '~lock' in filename:
				continue

			file_path = os.path.join(self.dir_input, filename)
			# read file into DataFrame
			df = pd.read_csv(file_path, usecols=self.COLS.keys())
			# Clean PID attribute
			df['PID'] = df['PID'].str.strip()
			# Add DataFrame to list of DataFrames
			self.dfs.append(df)

		self._concat_df()

	def _concat_df(self):
		self.data = pd.concat(self.dfs)
		# Rename columns
		self.data = self.data.rename(columns=self.COLS)

	def save_csv(self):
		self.data.to_csv(self.dir_output+self.file_name, index=False)

class AddAttrs():
	DIR_DATA = './data/'
	DIR_OUTPUT = './data/processed/'
	PUMP_DATA_PATH = os.path.join(DIR_OUTPUT, 'PUMP_PRESSURE.csv')
	ALLOCATION_DATA_PATH = os.path.join(DIR_DATA, 'Alocacion.csv')
	WELLTEST_DATA_PATH = os.path.join(DIR_DATA, 'RAW_WELL_TEST_DATA.csv')

	# Columns to read
	PUMP_DATA_COLS = ['Pozo', 'Fecha', 'Pip', 'Rpm']
	ALLOCATION_DATA_COLS = ['PID', 'Macolla', 'yacimiento', 'sand']
	WELLTEST_DATA_COLS = ['WELL', 'ZONES', 'RESERVOIR']


	def add(self):
		# Pump historical data
		df1 = pd.read_csv(self.PUMP_DATA_PATH, usecols=self.PUMP_DATA_COLS, decimal=",")

		# Welltest data
		df2 = pd.read_csv(self.WELLTEST_DATA_PATH, usecols=self.WELLTEST_DATA_COLS)
		df2 = df2.rename(
			columns={'WELL': 'Pozo','ZONES': 'Arena','RESERVOIR': 'Yacimiento'}
		)
		df2['Pozo'] = df2['Pozo'].str.replace(" ","-")

		# Allocation data
		df3 = pd.read_csv(self.ALLOCATION_DATA_PATH, usecols=self.ALLOCATION_DATA_COLS)
		df3 = df3.rename(
			columns={
				'PID': 'Pozo',
				'sand': 'Arena',
				'yacimiento': 'Yacimiento',
				'Macolla': 'Macolla',
			}
		)
		df3['Arena'] = df3['Arena'].str.replace(" ","")
		df3['Arena'] = "AMV-" + df3['Arena']

		df2 = df2.copy().drop_duplicates(subset=['Pozo'])
		df2 = df2.set_index('Pozo')

		df1.loc[df1['Pozo'] == 'I5-P9', 'Pozo'] = 'I5-P09'
		df1 = df1.copy().set_index('Pozo')

		self.data = df1.join(df2)
		self.data = self.data.reset_index().rename(columns={'index':'Pozo'})

		# Add zones to well with no zones (based on allocation data)
		no_zone = self.data.loc[self.data['Arena'].isnull()]
		wells_null = no_zone.Pozo.unique()
		length = len(wells_null)
		for i, well in enumerate(no_zone.Pozo.unique()):
			print("=============== {} =============== {}/{}".format(well, i, length))
			info = df3.loc[df3['Pozo'] == well].iloc[0]
			self.data.loc[self.data['Pozo'] == well, 'Arena'] = info['Arena']
			self.data.loc[self.data['Pozo'] == well, 'Yacimiento'] = info['Yacimiento']

		self.data['Macolla'] = self.data['Pozo'].str.split('-', expand=True)[0]
		self.data.loc[self.data['Yacimiento'] == 'SUP', 'Yacimiento'] = 'Superior'
		self.data.loc[self.data['Yacimiento'] == 'MID', 'Yacimiento'] = 'Medio'
		self.data.loc[self.data['Yacimiento'] == 'INF', 'Yacimiento'] = 'Inferior'
		
	def save_csv(self):
		# Save into csv file
		self.data.to_csv(self.DIR_OUTPUT+"PUMP_DATA_FULL_ATTRS.csv", index=False)

